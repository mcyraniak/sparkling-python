# -*- coding: utf-8 -*-


def snafu(foobar):
    """
    Since in python you don't declare type of parameters, they can me what they want.
    That's why you don't have function overloading (although I long for it every time I must write a massive
    function to cover all cases).
    """
    print "{}, Kilroy was here.".format(foobar.name)
    print "class of parameter foobar: {}\n".format(foobar.__class__.__name__)
