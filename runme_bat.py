# -*- coding: utf-8 -*-
from snafu import snafu


class Foo:
    def __init__(self, name):
        self.name = name
        print "{} initialized. It's class is {}".format(self.name, self.__class__.__name__)


class Bar:
    def __init__(self, name):
        self.name = name
        print "{} initialized. It's class is {}".format(self.name, self.__class__.__name__)


sparks = Foo("Sparks")
darkchozo = Bar("darkChozo")
print ""
snafu(sparks)
snafu(darkchozo)
